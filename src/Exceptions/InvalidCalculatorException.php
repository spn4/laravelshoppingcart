<?php

namespace SpondonIt\Shoppingcart\Exceptions;

use RuntimeException;

class InvalidCalculatorException extends RuntimeException
{
}
