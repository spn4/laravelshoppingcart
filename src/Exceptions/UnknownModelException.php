<?php

namespace SpondonIt\Shoppingcart\Exceptions;

use RuntimeException;

class UnknownModelException extends RuntimeException
{
}
