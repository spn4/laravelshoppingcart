<?php

namespace SpondonIt\Shoppingcart\Exceptions;

use RuntimeException;

class InvalidRowIDException extends RuntimeException
{
}
