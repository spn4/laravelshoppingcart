<?php

namespace SpondonIt\Shoppingcart\Exceptions;

use RuntimeException;

class CartAlreadyStoredException extends RuntimeException
{
}
