<?php

namespace SpondonIt\Shoppingcart\Contracts;

use SpondonIt\Shoppingcart\CartItem;

interface Calculator
{
    public static function getAttribute(string $attribute, CartItem $cartItem);
}
